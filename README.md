# senla_stage2
Tasks:

1. nginx as a tomcat reverse proxy server 
https://tomcat.example.com 
2. Create a pipeline: github (or gitlab) -> ansible playbook -> build and deploy java app -> send a status notification to the mail ( ccentre@inbox.ru )
3. Install and configure a solution that will monitor the nginx and tomcat services, if one of these services fails - report the incident to the mail  ccentre@inbox.ru 


---
## Getting started
First of all add `127.0.0.1 tomcat.example.com` to the /etc/hosts

then run:
`docker-compose up -d` 

start nginx + tomcat stack

files:
- docker-compose.yml
- server.xml
- default.conf


---
## Java app pipeline
files:
- pom.xml
- .gitlab-ci.yml
- /src
- /building